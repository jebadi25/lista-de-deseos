import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//routing
import { Routes, RouterModule } from '@angular/router';
//formularios
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './component/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './component/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './component/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './component/form-destino-viaje/form-destino-viaje.component';

//--
import { DestinosApiClient } from './models/destinos-api-client.model';
//redux
import { StoreModule as NgRxStoreModule, ActionReducerMap } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import {
  DestinosViajesState,
  intializeDestinosViajesState,
  reducerDestinosViajes,
  DestinosViajesEffects
} from './models/destinos-viajes-state.model';
import { LoginComponent } from './component/login/login/login.component';
import { ProtectedComponent } from './component/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
//routing
const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component:ListaDestinosComponent},
  {path: 'login', component: LoginComponent},
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]
  },
];

//redux init
export interface AppState {
  destinos: DestinosViajesState;
};

const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
};

let reducersInitialState = {
    destinos: intializeDestinosViajesState()
};
//fin redux init

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,                //formulario
    ReactiveFormsModule,        //formulario
    RouterModule.forRoot(routes), //routing
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState,
    runtimeChecks:{
      strictStateImmutability: false,
      strictActionImmutability: false,
    } 
    }),//redux
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument()
  ],
  providers: [
    DestinosApiClient, AuthService, UsuarioLogueadoGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

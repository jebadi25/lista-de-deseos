import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject} from 'rxjs';

//redux
import {Injectable} from '@angular/core';
import { tap, last } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Store } from '@ngrx/store';
import {
		DestinosViajesState,
		NuevoDestinoAction,
		ElegidoFavoritoAction
	} from './destinos-viajes-state.model';
import {AppState} from './../app.module';

@Injectable()
export class DestinosApiClient {

	constructor(private store: Store<AppState>) {
	}

    add(d: DestinoViaje){
        this.store.dispatch(new NuevoDestinoAction(d));
    }




    elegir(d: DestinoViaje){
        this.store.dispatch(new ElegidoFavoritoAction(d));
    }


}